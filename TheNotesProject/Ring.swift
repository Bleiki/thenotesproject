//
//  Ring.swift
//  TheNotesProject
//
//  Created by Admin on 26.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Ring: Object {
    
    dynamic var tag = Int()
    dynamic var text = ""
    
    override static func primaryKey() -> String? {
        return "tag"
    }
    
    init(tag: Int, text: String) {
        super.init()
        
        self.tag  = tag
        self.text = text
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init() {
        super.init()
    }
}
