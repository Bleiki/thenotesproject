//
//  RingsTableViewController.swift
//  TheNotesProject
//
//  Created by Admin on 26.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class RingsTableViewController: ProfileTableViewController {

    //MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DataManager.handsSections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return DataManager.handsSections[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.fingersOnHand.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let tag  = tagFromIndexPath(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileCell
        
        cell.leftLabel.text          = DataManager.fingersOnHand[indexPath.row]
        cell.rightTextField.tag      = tag
        cell.rightTextField.text     = DataManager.getRingWith(tag: tag)?.text
        cell.leftImageView.image     = #imageLiteral(resourceName: "Ring")
        cell.rightTextField.delegate = self

        
        return cell
    }
    
    //MARK: - UITextFieldDelegate    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        let ring = Ring(tag: textField.tag, text: textField.text ?? "")
        DataManager.saveRing(ring: ring)
        textField.resignFirstResponder()
    }
}
