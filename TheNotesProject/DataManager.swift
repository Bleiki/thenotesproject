//
//  DataManager.swift
//  TheNotesProject
//
//  Created by Admin on 15.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import RealmSwift

let realm = try! Realm()

class DataManager {
    
    //MARK: - Constants
    static let manGender              = "Man"
    static let womanGender            = "Woman"
    static let handsSections          = ["Правая рука", "Левая рука"]
    static let fingersOnHand          = ["Большой", "Указатеьный", "Средний", "Безымянный", "Мизинец"]
    static let profileSections        = ["Размеры", "Параметры"]
    static let profileParams          = ["Рост", "Ширина плеч",  "Длина рук", "Длина ног","Обхват головы", "Обхват шеи", "Обхват груди", "Обхват талии", "Обхват бедер"]
    static let manProfileImages       = [#imageLiteral(resourceName: "Headress"), #imageLiteral(resourceName: "ManOutwear"), #imageLiteral(resourceName: "Shirt"), #imageLiteral(resourceName: "ManPants"),  #imageLiteral(resourceName: "ManBoots"), #imageLiteral(resourceName: "ManUnderwear"), #imageLiteral(resourceName: "Bracelet"), #imageLiteral(resourceName: "Ring")]
    static let womanProfileImages     = [#imageLiteral(resourceName: "Headress"), #imageLiteral(resourceName: "WomanOuterwear"), #imageLiteral(resourceName: "WomanDress"), #imageLiteral(resourceName: "WomanPants"), #imageLiteral(resourceName: "WomanBoots"), #imageLiteral(resourceName: "Bra"), #imageLiteral(resourceName: "WomanUnderwear"), #imageLiteral(resourceName: "Bracelet"), #imageLiteral(resourceName: "Ring")]
    static let manProfileDimensions   = ["Головной убор", "Верхняя одежда", "Рубашка, футболка", "Брюки, шорты", "Обувь", "Нижнее  белье", "Браслет", "Кольца"]
    static let womanProfileDimensions = ["Головной убор", "Верхняя одежда", "Платье, блузка", "Юбка, брюки", "Обувь", "Бюстгалтер", "Нижнее белье", "Браслет", "Кольца"]
    
    //MARK: - RealmGetMethods
    class func getPartOfProfileWith(tag: Int) -> Profile? {
        return realm.object(ofType: Profile.self, forPrimaryKey: tag)
    }
    
    class func getUserGender() -> User? {
        return realm.objects(User.self).first
    }
    
    class func getRingWith(tag: Int) -> Ring? {
        return realm.object(ofType: Ring.self, forPrimaryKey: tag)
    }
    
    //MARK: - RealmSaveMethods
    class func saveUserGender(user: User) {
        do {
            try realm.write {
                realm.add(user)
            }
        } catch {
            print("Can't save user gender!!!")
        }
    }
    
    class func saveRing(ring: Ring) {
        do {
            try realm.write {
                realm.add(ring, update: true)
            }
        } catch {
            print("Can't save ring!!!")
        }
    }
    
    class func savePartOfProfile(part: Profile) {
        do {
            try realm.write {
                realm.add(part, update: true)
            }
        } catch {
            print("Can't save part of profile!!!")
        }
    }
}
