//
//  ProfileControllerFunctions.swift
//  TheNotesProject
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import UIKit

extension ProfileTableViewController {
    
    //MARK: - TagFunction
    // Get tag for textField identification.
    func tagFromIndexPath(indexPath: IndexPath) -> Int {
        return indexPath.section * 1000000 + indexPath.row + 1
    }

    //MARK: - ConfigureFunctions
    // Choose dimensions and images.
    func setDimensionsAndImagesForProfile() {
        if let user = DataManager.getUserGender() {
            if user.gender == DataManager.manGender {
                self.profileDimensions = DataManager.manProfileDimensions
                self.profileImages     = DataManager.manProfileImages
            } else {
                self.profileDimensions = DataManager.womanProfileDimensions
                self.profileImages     = DataManager.womanProfileImages
            }
        }
    }

    // Set gradients for some parts.
    func setGradientsForProfile() {
        let zeroRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        self.navigationController?.navigationBar.layer.insertSublayer(CALayer.profileHorizontalGradientLayer(rect: self.navigationController?.navigationBar.bounds ?? zeroRect), at: 0)
        
        self.tabBarController?.tabBar.layer.insertSublayer(CALayer.profileHorizontalGradientLayer(rect: self.tabBarController?.tabBar.bounds ?? zeroRect), at: 0)

        self.tableView.backgroundView = UIView(frame: self.view.bounds)
        self.tableView.backgroundView?.layer.insertSublayer(CALayer.profileHorizontalGradientLayer(rect: self.tableView.backgroundView?.bounds ?? zeroRect), at: 1)
    }

    //MARK: - SubscriptionFunctions
    func subscriptionsForProfile() {
        
        // For work with animation.
        if self.firstAppearance {
            self.visibleRowsAtFirstAppear = self.tableView.indexPathsForVisibleRows
        }
        
        //  Subscribed to notifications.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Create gesture recognizer.
        if self.keyboardDismissTapGesture == nil {
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }

    // Unsubscribe from notifications and gestureRecognizer.
    func unsubscriptionsForProfile() {
        
        NotificationCenter.default.removeObserver(self)
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    
    //MARK: - KeyboardFunctions
    // Content offset, when appearing keyboard.
    func keyboardWillShow(notification: NSNotification) {
        
        let rowHeight           = self.tableView.rowHeight
        let keyboardSize        = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect
        let backgroundView      = self.tableView.backgroundView
        let tapOnBackgroundView = keyboardDismissTapGesture?.location(in: backgroundView)
        var offsetForKeyboard   = backgroundView?.bounds
        
        if keyboardSize != nil, backgroundView != nil {
            offsetForKeyboard?.origin.y    = backgroundView!.bounds.maxY - keyboardSize!.height - rowHeight
            offsetForKeyboard?.size.height = keyboardSize!.height + rowHeight
        }
        
        if self.view.frame.origin.y == 0, let offset = offsetForKeyboard, let tap = tapOnBackgroundView, offset.contains(tap) {
            self.view.frame.origin.y -= offset.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0 // Content offset, when disappearing keyboard.
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true) // Dismiss keyboard by gestureRecognizer.
    }
}
