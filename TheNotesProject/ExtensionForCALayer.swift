//
//  ExtensionForCALayer.swift
//  TheNotesProject
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    
    //MARK: - ProfileGradientLayerFunctions
    // Create horizontal gradient layer for profile.
    class func profileHorizontalGradientLayer(rect: CGRect) -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        let leftColor     = UIColor(colorLiteralRed: 0.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1)
        let rightColor    = UIColor(colorLiteralRed: 51.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1)
        let colors        = [leftColor.cgColor, rightColor.cgColor]
        
        gradientLayer.frame      = rect
        gradientLayer.colors     = colors
        gradientLayer.endPoint   = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        return gradientLayer
    }
    
    // Create vertical gradient layer for profile.
    class func profileVerticalGradientLayer(rect: CGRect) -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        let topColor      = UIColor(colorLiteralRed: 0.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1)
        let bottomColor   = UIColor(colorLiteralRed: 51.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1)
        let colors        = [topColor.cgColor, bottomColor.cgColor]
        
        gradientLayer.frame      = rect
        gradientLayer.colors     = colors
        gradientLayer.endPoint   = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        
        return gradientLayer
    }
}
