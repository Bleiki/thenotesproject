//
//  RingOfProfileTableViewCell.swift
//  TheNotesProject
//
//  Created by Admin on 26.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class RingOfProfileTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    
    //MARK: - LifeCycle
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        configureCell()
    }
    
    //MARK: - ConfigFunc
    func configureCell() {

        self.gradientView.layer.addSublayer(CALayer.profileVerticalGradientLayer(rect: self.gradientView.bounds))
        
        self.contentView.bringSubview(toFront: self.gradientView)
        self.gradientView.bringSubview(toFront: self.leftLabel)
        self.gradientView.bringSubview(toFront: self.leftImageView)
        self.gradientView.bringSubview(toFront: self.rightImageView)
    }
}
