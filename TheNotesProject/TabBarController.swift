//
//  TabBarController.swift
//  TheNotesProject
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

class TabBarController: UITabBarController {
    
    //MARK: - LifeCycle
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.tabBar.tintColor = .black
        self.tabBar.unselectedItemTintColor = UIColor(red: 96.0 / 255.0, green: 89.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0)

        if DataManager.getUserGender() == nil {
            presentGenderAlertController() // Add alert for choose user gender.
        }
    }
    
    //MARK: - AlertFunction
    func presentGenderAlertController() {
        
        let alertController = UIAlertController(title: "Вход", message: "Укажите свой пол для работы с профилем.", preferredStyle: .alert)
        let alertManAction  = UIAlertAction(title: "Мужчина", style: .default, handler: { (action) in
            let user = User()
            user.gender = DataManager.manGender
            DataManager.saveUserGender(user: user)
        })
        let alertWomanAction = UIAlertAction(title: "Женщина", style: .default, handler: { (action) in
            let user = User()
            user.gender = DataManager.womanGender
            DataManager.saveUserGender(user: user)
        })
        
        alertController.addAction(alertManAction)
        alertController.addAction(alertWomanAction)
        present(alertController, animated: true, completion: nil)
    }
}
