//
//  ProfileCell.swift
//  TheNotesProject
//
//  Created by Admin on 30.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var leftImageView: UIImageView!
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    //MARK: - ConfigFunc
    func configureCell() {
        
        gradientView.layer.addSublayer(CALayer.profileVerticalGradientLayer(rect: gradientView.bounds))
        
        rightTextField.layer.cornerRadius = rightTextField.bounds.height / 2
        rightTextField.layer.borderColor  = UIColor.black.cgColor
        rightTextField.layer.borderWidth  = 1.2
        
        contentView.bringSubview(toFront: gradientView)
        gradientView.bringSubview(toFront: rightTextField)
        gradientView.bringSubview(toFront: leftLabel)
        gradientView.bringSubview(toFront: leftImageView)
    }
}
