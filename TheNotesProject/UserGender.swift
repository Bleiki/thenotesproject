//
//  UserGender.swift
//  TheNotesProject
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class User: Object {
    dynamic var gender: String?
}
