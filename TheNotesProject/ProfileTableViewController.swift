//
//  ProfileTableViewController.swift
//  TheNotesProject
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController, UITextFieldDelegate {
    
    //MARK: - ConsantsAndVariables
    let sectionOfDimensions                = 0
    let profileCellNibName                 = "ProfileCell"
    let profileCellIdentifier              = "ProfileCell"
    let ringOfProfileCellIdentifier        = "RingOfProfileCell"
    let ringsTableViewControllerIdentifier = "RingsTableViewController"
    
    var firstAppearance          = true
    var profileImages            : [UIImage]?
    var profileDimensions        : [String]?
    var visibleRowsAtFirstAppear : [IndexPath]?
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !(self is RingsTableViewController) {
            self.setDimensionsAndImagesForProfile()
        }
        
        let nib = UINib(nibName: profileCellNibName, bundle: Bundle.main) // Create nib for work with cell from xib.
        
        self.tableView.register(nib, forCellReuseIdentifier: profileCellIdentifier) // Register cell from xib for work.
        self.tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.setGradientsForProfile()
        self.subscriptionsForProfile()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.unsubscriptionsForProfile()
    }

    //MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DataManager.profileSections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return DataManager.profileSections[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionOfDimensions {
            return profileDimensions?.count ?? 0
        } else {
            return DataManager.profileParams.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tag = self.tagFromIndexPath(indexPath: indexPath)

        if let dimensions = profileDimensions, indexPath.section == sectionOfDimensions, indexPath.row == dimensions.count - 1 {
            
            // Cell for ring
            let cell = tableView.dequeueReusableCell(withIdentifier: ringOfProfileCellIdentifier, for: indexPath) as! RingOfProfileTableViewCell
            
            cell.leftLabel.text       = dimensions[indexPath.row]
            cell.leftImageView.image  = profileImages?[indexPath.row]
            cell.rightImageView.image = #imageLiteral(resourceName: "Forward")
            
            return cell
            
        } else {

            // Other cells.
            let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileCell
            
            cell.rightTextField.tag      = tag
            cell.rightTextField.text     = DataManager.getPartOfProfileWith(tag: tag)?.text
            cell.rightTextField.delegate = self
            
            if indexPath.section == sectionOfDimensions {
                cell.leftLabel.text      = profileDimensions?[indexPath.row]
                cell.leftImageView.image = profileImages?[indexPath.row]
            } else {
                cell.leftLabel.text      = DataManager.profileParams[indexPath.row]
                cell.leftImageView.image = #imageLiteral(resourceName: "Meter")
            }
            
            return cell
        }
    }
    
    //MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.layer.insertSublayer(CALayer.profileHorizontalGradientLayer(rect: view.bounds), at: 1) // Set gradient for header.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let isProfileController = !(self is RingsTableViewController)
        
        if isProfileController, let dimensions = profileDimensions, indexPath.section == sectionOfDimensions, indexPath.row == dimensions.count - 1 {
            let ringsVC = self.storyboard?.instantiateViewController(withIdentifier: ringsTableViewControllerIdentifier) as! RingsTableViewController
            self.navigationController?.pushViewController(ringsVC, animated: true) // Transition to RingsTableViewController
        }
    }
    
    // Add animation.
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let numberOfRowsInSection0 = tableView.numberOfRows(inSection: 0)
        let animationDuration      = 0.8
        let animationDelay         = firstAppearance ? 0.05 * Double(indexPath.row + indexPath.section * numberOfRowsInSection0) : 0
        let XTranslation           = -tableView.bounds.width
        let translationTransform   = CATransform3DMakeTranslation(XTranslation, 0, 0)
        let resultTransform        = CATransform3DScale(translationTransform, 0, 0, 1)
        
        cell.layer.transform = resultTransform
        
        UIView.animate(withDuration: animationDuration, delay: animationDelay, options: .curveEaseInOut, animations: {
            cell.layer.transform = CATransform3DIdentity
        })
        
        if let rows = self.visibleRowsAtFirstAppear, indexPath == rows.last {
            self.firstAppearance = false
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let partOfProfile = Profile(tag: textField.tag, text: textField.text ?? "")
        DataManager.savePartOfProfile(part: partOfProfile)
        textField.resignFirstResponder()
    }
}
